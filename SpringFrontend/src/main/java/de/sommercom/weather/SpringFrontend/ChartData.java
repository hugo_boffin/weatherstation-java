package de.sommercom.weather.SpringFrontend;

import com.datastax.driver.core.*;

import java.sql.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by christian on 3/11/2015.
 */
public class ChartData {
    private Date date;

    private final ArrayList<Integer> xAxis = new ArrayList<Integer>();
    private final ArrayList<Float> yHumidity = new ArrayList<Float>();
    private final ArrayList<Float> yTemperature = new ArrayList<Float>();
    private final ArrayList<float[]> humidityRange = new ArrayList<float[]>();
    private final ArrayList<float[]> temperatureRange = new ArrayList<float[]>();

    public ChartData(String dateString) {
        if(dateString == "today" )
                date = new Date(System.currentTimeMillis());
        else {
            DateFormat format = new SimpleDateFormat("d-MM-yyyy", Locale.ENGLISH);
            try {
                date = format.parse(dateString);
            } catch (ParseException e) {
                date = new Date(System.currentTimeMillis());
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        //getDataFromNeo4j(day, month, year);
        //getDataFromCassandra(day, month, year);
        getDataFromSqlServer(day, month, year);
    }

    private void getDataFromSqlServer(int day, int month, int year) {
        try {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            final String query = "SELECT  \n" +
                    "\tDATEPART(HOUR, eventtime) as hour\n" +
                    "      ,MIN([Temperature]) as min_tmp\n" +
                    "      ,MIN([Humidity])  as min_hum\n" +
                    "      ,AVG( Cast([Temperature] as Float)) as avg_tmp\n" +
                    "      ,AVG( Cast([Humidity] as Float)) as avg_hum\n" +
                    "      ,MAX([Temperature]) as max_tmp\n" +
                    "      ,MAX([Humidity]) as max_hum\n" +
                    "\t  ,COUNT(*) as cnt\n" +
                    "  FROM [dbo].[SensorData]\n" +
                    "WHERE eventdate = DATEFROMPARTS("+year+", "+month+", "+day+")\n" +
                    "Group by DATEPART(HOUR, eventtime)\n" +
                    "ORDER by DATEPART(HOUR, eventtime)";

            String connectionUrl = Config.sqlServerConnectionString;
            Connection con = DriverManager.getConnection(connectionUrl);
            Statement stmt = con.createStatement();
            final ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                xAxis.add(rs.getInt("hour"));
                yTemperature.add(rs.getFloat("avg_tmp"));
                yHumidity.add(rs.getFloat("avg_hum"));
                humidityRange.add(new float[]{/*rs.getInt("hour"),*/rs.getFloat("min_hum"),rs.getFloat("max_hum")});
                temperatureRange.add(new float[]{/*rs.getInt("hour"),*/rs.getFloat("min_tmp"),rs.getFloat("max_tmp")});
            }
            stmt.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String dateStr(int d, int m, int y) {
        //2015-05-21 00:00:00
        return String.format("%04d-%02d-%02d",y,m,d);
    }

    private void getDataFromCassandra(int day, int month, int year) {
        String dateS = ChartData.dateStr(day, month, year);

        Cluster cluster = Cluster.builder().addContactPoint("LANTIA").build();
        Session session = cluster.connect("\"WeatherStation\"");
        com.datastax.driver.core.PreparedStatement statement = session.prepare(
                "SELECT time,hum,tmp FROM \"SensorData\" WHERE station_id = 0 AND time >= '"+dateS+" 00:00:00' AND time <= '"+dateS+" 23:59:59'");
        BoundStatement boundStatement = new BoundStatement(statement);
        com.datastax.driver.core.ResultSet results = session.execute(boundStatement.bind( ));

        HashMap<Integer, SensorHourAggregate> map = new HashMap<Integer, ChartData.SensorHourAggregate>();
        for (Row row : results) {
            Date date = row.getDate("time");
            final int hour = date.getHours();
            if (map.get(hour) == null) {
                map.put(hour, new ChartData.SensorHourAggregate());
            }
            map.get(hour).addData(row.getInt("tmp"),row.getInt("hum"));
        }
        session.close();
        cluster.close();

        for (int hour = 0; hour < 24; hour++) {
            ChartData.SensorHourAggregate agg = map.get(hour);
            if (agg != null) {
                xAxis.add(hour);
                yTemperature.add(agg.getTmpAvg());
                yHumidity.add(agg.getHumAvg());
                humidityRange.add(new float[]{/*rs.getInt("hour"),*/agg.getHumMin(),agg.getHumMax()});
                temperatureRange.add(new float[]{/*rs.getInt("hour"),*/agg.getTmpMin(),agg.getTmpMax()});
            }
        }
    }

    private void getDataFromNeo4j(int day, int month, int year) {
        try {
            try {
                Class.forName("org.neo4j.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            final String query = "MATCH (sa:SensorData) -[:HAS_SENSOR_DATA]->(s:Second)<-[:CHILD]-(m:Minute)<-[:CHILD]-(h:Hour)<-[:CHILD]-(d:Day)<-[:CHILD]-(mo:Month)<-[:CHILD]-(y:Year)\n" +
                    "WHERE d.value = " + day + " AND mo.value = " + month + " AND y.value = " + year + "\n" +
                    "RETURN h.value as hour, MIN(sa.humidity) as min_hum,MIN(sa.temperature) as min_tmp, AVG(sa.humidity) as avg_hum,AVG(sa.temperature) as avg_tmp, MAX(sa.humidity) as max_hum,MAX(sa.temperature) as max_tmp\n" +
                    "ORDER BY h.value";

            Connection con = DriverManager.getConnection("jdbc:neo4j://lantia:7474/?debug=true");
            Statement stmt = con.createStatement();
            final ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                xAxis.add(rs.getInt("hour"));
                yTemperature.add(rs.getFloat("avg_tmp"));
                yHumidity.add(rs.getFloat("avg_hum"));
                humidityRange.add(new float[]{/*rs.getInt("hour"),*/rs.getFloat("min_hum"),rs.getFloat("max_hum")});
                temperatureRange.add(new float[]{/*rs.getInt("hour"),*/rs.getFloat("min_tmp"),rs.getFloat("max_tmp")});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Integer> getxAxis() {
        return xAxis;
    }

    public ArrayList<Float> getyHumidity() {
        return yHumidity;
    }

    public ArrayList<Float> getyTemperature() {
        return yTemperature;
    }

    public ArrayList<float[]> getHumidityRange() {
        return humidityRange;
    }

    public ArrayList<float[]> getTemperatureRange() {
        return temperatureRange;
    }

    public ArrayList<Float> getyDewPoint() {
        ArrayList<Float> dew = new ArrayList<Float>();
        for (int i = 0; i < yTemperature.size(); i++) {
            final float tmp = yTemperature.get(i);
            final float hum = yHumidity.get(i);

            dew.add(dewPoint(tmp,hum));
        }
        return dew;
    }

    // dewPoint function NOAA
    // reference (1) : http://wahiduddin.net/calc/density_algorithms.htm
    // reference (2) : http://www.colorado.edu/geography/weather_station/Geog_site/about.htm
    private float dewPoint(float celsius, float humidity)
    {
        // (1) Saturation Vapor Pressure = ESGG(T)
        float RATIO = 373.15f / (273.15f + celsius);
        float RHS = -7.90298f * (RATIO - 1f);
        RHS += 5.02808 * Math.log10(RATIO);
        RHS += -1.3816e-7 * (Math.pow(10, (11.344 * (1 - 1 / RATIO))) - 1) ;
        RHS += 8.1328e-3 * (Math.pow(10, (-3.49149 * (RATIO - 1))) - 1) ;
        RHS += Math.log10(1013.246);

        // factor -3 is to adjust units - Vapor Pressure SVP * humidity
        float VP = (float)Math.pow(10, RHS - 3) * humidity;

        // (2) DEWPOINT = F(Vapor Pressure)
        float T = (float)Math.log(VP / 0.61078);   // temp var
        return (241.88f * T) / (17.558f - T);
    }


    public Date getDate() {
        return date;
    }

    public static class SensorHourAggregate {
        private int hum_min;
        private int hum_sum;
        private int hum_max;
        private int tmp_min;
        private int tmp_sum;
        private int tmp_max;

        private int num_vals;

        public SensorHourAggregate() {
            tmp_min = hum_min = Integer.MAX_VALUE;
            tmp_max = hum_max = Integer.MIN_VALUE;

            tmp_sum = hum_sum = 0;
            num_vals = 0;
        }

        public void addData(int tmp, int hum) {
            if (tmp < tmp_min)
                tmp_min = tmp;
            if (hum < hum_min)
                hum_min = hum;
            if (tmp > tmp_max)
                tmp_max = tmp;
            if (hum > hum_max)
                hum_max = hum;

            tmp_sum += tmp;
            hum_sum += hum;

            num_vals++;
        }

        public float getTmpAvg() {
            return (float)tmp_sum / (float)num_vals;
        }

        public float getHumAvg() {
            return (float)hum_sum / (float)num_vals;
        }

        public int getHumMin() {
            return hum_min;
        }

        public int getHumMax() {
            return hum_max;
        }

        public int getTmpMin() {
            return tmp_min;
        }

        public int getTmpMax() {
            return tmp_max;
        }

        @Override
        public String toString() {
            return "SensorHourAggregate{" +
                    "hum_min=" + hum_min +
                    ", hum_avg=" +  getHumAvg() +
                    ", hum_max=" + hum_max +
                    ", tmp_min=" + tmp_min +
                    ", tmp_avg=" +  getTmpAvg() +
                    ", tmp_max=" + tmp_max +
                    ", num_vals=" + num_vals +
                    '}';
        }
    }
}
