package de.sommercom.weather.SpringFrontend;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Controller
public class MainController {

    @RequestMapping("/")
    public String main(@RequestParam(value = "date", required = false, defaultValue = "today") String date, Model model) {
        Date d;
        if(date == "today" )
            d = new Date(System.currentTimeMillis());
        else {
            DateFormat format = new SimpleDateFormat("d-MM-yyyy", Locale.ENGLISH);
            try {
                d = format.parse(date);
            } catch (ParseException e) {
                d = new Date(System.currentTimeMillis());
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        model.addAttribute("date", cal.getTimeInMillis());

        return "main";
    }
}