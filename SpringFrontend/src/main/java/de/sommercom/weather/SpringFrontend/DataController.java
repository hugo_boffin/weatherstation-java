package de.sommercom.weather.SpringFrontend;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by christian on 3/11/2015.
 */
@RestController
public class DataController {

    @RequestMapping(value="/data", method = RequestMethod.GET)
    public ChartData data(@RequestParam(value = "date", required = false, defaultValue = "today") String date) {
        return new ChartData(date);
    }
}
