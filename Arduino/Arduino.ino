#include <dht11.h>

dht11 DHT11;

const int DHT11PIN = 5;

void setup()
{
  Serial.begin(9600);
}

void loop()
{  
  int chk = DHT11.read(DHT11PIN);

  Serial.print(">>");

  switch (chk)
  {
    case DHTLIB_OK: 
		Serial.print("DATA,"); 
                Serial.print(DHT11.humidity);
                Serial.print(",");
                Serial.print(DHT11.temperature);
		break;
    case DHTLIB_ERROR_CHECKSUM: 
		Serial.print("ERROR,CHECKSUM"); 
		break;
    case DHTLIB_ERROR_TIMEOUT: 
		Serial.print("ERROR,TIMEOUT"); 
		break;
    default: 
		Serial.print("ERROR,UNKNOWN"); 
		break;
  }

  Serial.println("<<");

  delay(10000);
}
