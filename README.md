# README #

This is my weather station project. It consists of the following modules:

1. An Arduino sketch - The Arduino Uno collects data from an DHT11 temperature and humidity sensor und writes its data to the serial port.
2. SerialLogger - A service/daemon that listens on the serial port and pushes new data to a SQL/neo4j/cassandra/... data store.
3. WebFrontend - A Spring Web Frontend utilizing Thymeleaf, Bootstrap and Highcharts for a nice view.

### How does it look? ###

![weatherstation.png](https://bitbucket.org/repo/L8GpLL/images/827062389-weatherstation.png)

### How do I get set up? ###

Hit mvn install and wait

### Contribution guidelines ###

* None yet

### Who do I talk to? ###

* hugo_boffin