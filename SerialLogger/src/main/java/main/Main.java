package main;

import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Date;

//TODO: Some kind of parity bit / checksum for serial reading
//TODO: Pop Mode - Request infos instead of polling them
//TODO: Use a nice serializer for JSON - jackson?
//TODO: add error handling
//mvn install:install-file -Dfile=sqljdbc41.jar -Dpackaging=jar -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc41 -Dversion=4.1
//mvn install:install-file -Dfile=../RXTXcomm.jar -DgroupId=org.rxtx -DartifactId=rxtxcomm -Dversion=2.2.0 -Dpackaging=jar
//http://fizzed.com/oss/rxtx-for-java
//TODO: Windows Service Mode -mnsm
//TODO: Logging in Service Mode (verbose)
//TODO: Failure safe logging - what happens when database is down?
//TODO: multiple sensor sources?
//TODO: small cache if db server dies/restarts?

/**
 * Created by christian on 3/8/2015.
 *
 * http://rxtx.qbang.org/wiki/index.php/Two_way_communcation_with_the_serial_port
 */
public class Main {
    public static void main(String[] args) {
        try {
            final CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier("COM3");
            final CommPort commPort = portIdentifier.open("READER",9600);

            if ( commPort instanceof SerialPort )
            {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);

                InputStream in = serialPort.getInputStream();

                (new Thread(new SerialReader(in))).start();
            }
        } catch (NoSuchPortException e) {
            e.printStackTrace();
        } catch (PortInUseException e) {
            e.printStackTrace();
        } catch (UnsupportedCommOperationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class SerialReader implements Runnable
    {
//        private Cluster cluster;
//        private Session session;
//        private PreparedStatement statement;

        private final InputStream in;

        public SerialReader ( InputStream in )
        {
            this.in = in;
        }

        public void run ()
        {
//            initCassandra();
            initSqlServer();

            StringBuilder builder = new StringBuilder(1024);
            byte[] buffer = new byte[1024];
            int len;
            try
            {
                while ( ( len = this.in.read(buffer)) > -1 )
                {
                    if (len > 0) {
                        builder.append(new String(buffer,0,len));

                        while (builder.indexOf("<<") != -1 ) {
                            int start = builder.indexOf(">>");
                            int stop = builder.indexOf("<<");
                            log(builder.substring(start+2,stop));
                            builder.delete(0,stop+2);
                        }
                    }
                }
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
//            cluster.close();
        }

        private void initSqlServer() {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

//        private void initCassandra() {
//            cluster = Cluster.builder().addContactPoint("LANTIA").build();
//            Metadata metadata = cluster.getMetadata();
//            System.out.println("Connected to cluster: " + metadata.getClusterName());
//            for ( Host host : metadata.getAllHosts() ) {
//                System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n",
//                        host.getDatacenter(), host.getAddress(), host.getRack());
//            }
//            session = cluster.connect("\"WeatherStation\"");
//            statement = session.prepare(
//                    "INSERT INTO \"SensorData\" (station_id,time,hum,tmp) VALUES(?,?,  ?,?)");
//        }

        private void log(String s) throws IOException {
            String[] params = s.split(",");

            if (!params[0].equals("DATA"))
                return;

            final int hum = Integer.valueOf(params[1]);
            final int tmp = Integer.valueOf(params[2]);
            final long time = System.currentTimeMillis();

            //logToCassandra(tmp, hum, time);
            //logToNeo4j(tmp, hum, time);
            logToSqlServer(tmp, hum, time);
        }

        private void logToSqlServer(int tmp, int hum, long time) {
            Date now = new Date(time);

            String query = "INSERT INTO dbo.SensorData (eventdate, eventtime, humidity, temperature)" +
                    "VALUES(?,?,?,?)";
            String connectionUrl = "jdbc:sqlserver://LANTIA;instanceName=SQLEXPRESS;integratedSecurity=false;User=sa;" +
                    "Password=BW4SDzWg;database=WeatherStation;connection timeout=15"; // TODO: in eine config file

            try {
                Connection con = DriverManager.getConnection(connectionUrl);

                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setDate(1, new java.sql.Date(now.getTime()));
                stmt.setTime(2, new Time(now.getTime()));
                stmt.setShort(3, (short)hum);
                stmt.setShort(4, (short)tmp);

                stmt.execute();

                stmt.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

//        private void logToCassandra(int tmp, int hum, long time) {
//            Date now = new Date(time);
//            BoundStatement boundStatement = new BoundStatement(statement);
//            session.execute(boundStatement.bind(0, now, hum, tmp));
//        }
//
//        private void logToNeo4j(int tmp, int hum, long time) {
//
//            final String humidity = hum+".0f";
//            final String temperature = tmp+".0f";
//
//            // Create SensorData Node
//
//            String query1 = "{\"statements\":[{\"statement\": \"CREATE (sa:SensorData {temperature:"+ temperature +", humidity:"+ humidity +"}) RETURN id(sa)\"}]}";
//            CloseableHttpClient httpClient = HttpClients.createDefault();
//
//            final HttpPost httpPost1 = new HttpPost("http://LANTIA:7474/db/data/transaction/commit");
//            httpPost1.setEntity(new StringEntity(query1, ContentType.APPLICATION_JSON));
//
//            try {
//                CloseableHttpResponse response1 = httpClient.execute(httpPost1);
//
//                try {
//                    HttpEntity entity1 = response1.getEntity();
//
//                    if (!response1.getStatusLine().toString().contains("200 OK"))
//                        return;
//
//                    final String result = IOUtils.toString(entity1.getContent(), Charset.defaultCharset());
//
//                    final int start = result.indexOf("\"row\":[");
//                    final int stop = result.indexOf("]}]}]");
//                    String nodeId = result.substring(start+7,stop);
//                    EntityUtils.consume(entity1);
//
//                    // Create Time Node and connect to SensorData
//                    String query2 = "{\"nodeId\": "+nodeId+",\"relationshipType\": \"HAS_SENSOR_DATA\",\"timezone\": \"GMT+1\",\"resolution\": \"SECOND\",\"time\": "+time+"}";
//
//                    HttpPost httpPost2 = new HttpPost("http://LANTIA:7474/graphaware/timetree/single/event");
//                    httpPost2.setEntity(new StringEntity(query2, ContentType.APPLICATION_JSON));
//                    CloseableHttpResponse response2 = httpClient.execute(httpPost2);
//
//                    try {
//                        HttpEntity entity2 = response2.getEntity();
//                        EntityUtils.consume(entity2);
//                    } finally {
//                        response2.close();
//                        if (response2.getStatusLine().toString().contains("201 Created"))
//                            System.out.print(".");
//                    }
//                } finally {
//                    response1.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }
}
